package fbalm.checkpoints.radioactivedecay;

import java.util.Arrays;
import java.util.Scanner;

import org.apache.commons.math3.stat.regression.SimpleRegression;
/**
 * Main class to run the basic, one-particle checkpoint. Every n (default 30) seconds, the number of nucle� 
 * that have not decayed yet is counted, and the data is added to a regression. This regression is the linearized
 * data, which in turn is used 
 * @author Floris Balm
 *
 */
public class RadioactiveMain{
    
    //Dataset to get the halflife from 
    private SimpleRegression dataset;
    //2D array for the particles
    private RadioactiveParticle[][] grid;
    //Number of particles that have not decayed in the grid
    private int numberNotDecayed;
    //Time between collections
    private int dataCollectionInterval;
    
    /**
     * Constructor for the RadioactiveMain simulation
     * @param gridSize Size for the NxN grid
     * @param decayConstant l in the expression N = N0 e^-lt
     */
    public RadioactiveMain(int gridSize, double decayConstant, int dataCollectionInterval)
    {
        //Construct the 2D array
        grid = new RadioactiveParticle[gridSize][gridSize];
        
        //Dataset that does not have to go through the origin
        dataset = new SimpleRegression(true);
        
        //Collect data every 30 seconds
        this.dataCollectionInterval = dataCollectionInterval;
        //Number of particles at the start
        numberNotDecayed = gridSize*gridSize;
        
        RadioactiveParticle.setDecayConstant(decayConstant);        
        
        //Initialize grid to particles
        for(int i = 0; i<gridSize; ++i)
        {
            for(int j = 0; j<gridSize; ++j){
                grid[i][j] = new RadioactiveParticle();
            }
        }
    }
    /**
     * Print the grid in its current state
     * @param time The current timestamp of the simulation
     */
    public void printGrid(int time)
    {
        //Timestamp and number of particles yet to decay
        System.out.printf("Particles at t = %d seconds. There are %d particles yet to decay.\n", time,numberNotDecayed);
        
        for(int i = 0; i<grid.length; ++i)
        {
            System.out.println(Arrays.toString(grid[i]));
        }
        
        System.out.println();
    }
    
    
    /**
     * Get the particle at an index in the grid
     * @param i first index of the particle
     * @param j second index of the particle
     * @return The particle at (i,j) in the array or null if null.
     */
    public RadioactiveParticle getParticle(int i, int j)
    {
        //Check for bounds, has to be 0 <= i,j < gridsize
        if(i<0 || i>=grid.length || j<0 || j>=grid[0].length)
        {
            return null;
        } else {
            //Return a copy of the selected particle
            return grid[i][j].clone();
        }
    }
    /**
     * Run the simulation
     * @param speedFactor Factor to speed the simulation up by from real time
     */
    public void run(double speedFactor)
    {
        
        //Counter to prevent the grid from printing every few seconds.
        int loopCounter = 0;
        
        //Count the number of times a point has been added to the data set. 
        int datasetCounter = 0;

        long interval = 1000L;

        //If there are still particles that have not decayed, and max of 40 points added to the dataset
        while (numberNotDecayed != 0 && datasetCounter < 40)
        {
            
            //Every 30 "seconds" the timer counts and prints the grid. Actual time depends on speed factor
            if(loopCounter % 30 == 0)
            {
                //Reset the loopcounter for the next print
                loopCounter = 0;
                
                //The current time in the simulation is 
                int time = datasetCounter*dataCollectionInterval;
                printGrid(time);
                
                //Datapoints to be added to the dataset.
                //The log is taken to linearize the data to ln(N) = ln(N0) - lt.
                double xCoord = (double)(time);
                double yCoord = Math.log((double)numberNotDecayed);
                dataset.addData(xCoord, yCoord);

                
                datasetCounter++;
            }
            //Do the next decay step on the grid
            decayStep(interval);
            
            ++loopCounter;
            
            try{
                //Speed the sleep interval up by a factor of speedFactor
                long sleepDuration = (long) ((double)interval/speedFactor);
                
                Thread.sleep(sleepDuration);
            } catch (InterruptedException e){
                e.printStackTrace(System.err); //Report error to System.err
            }
        }

        printResults();
    }
    
    
    /**
     * Performs the next step in the interval. On this interval, each particle has an identical probability
     * of decaying. Each particle will be looped through and checked if it decays
     * @param interval The timeinterval each particle has to decay. Usually roughly a second, can be changed.
     */
    public void decayStep(double interval)
    {
        //loop through the grid
        for(int i = 0; i<grid.length; i++)
        {
            for(int j = 0; j<grid[i].length; j++)
            {
                RadioactiveParticle p = grid[i][j];
                
                //If a particle has not decayed
                if (!p.hasDecayed())
                {
                    //Calls the decays method on the current particle.
                    //If a particle that hasn't decayed decays in this step,
                    //the numberNotDecayed counter is updated. When there is nothing left to decay, loop ends
                    if(p.decays(((double)interval)/1000.0))
                    {
                        --numberNotDecayed;
                    }
                }
            }
        }

    }
    /**
     * Prints the results from the dataset. The dataset was linearized as log(N) = log(N0) - lt. Then the gradient
     * of the regression is equal to -lambda.
     */
    public void printResults()
    {
        
        System.out.printf("The slope of the line is %f.\n", dataset.getSlope());
        
        //halflife = ln(2)/l
        double halfLife = -Math.log(2.0)/dataset.getSlope();
        System.out.printf("This corresponds to a halflife of %.3f.\n", halfLife);

        //The error on halflife is given by dt = ln(2)/(l^2) * dl
        double halfLifeError = Math.abs(dataset.getSlopeStdErr() * Math.log(2.0)*Math.pow(dataset.getSlope(), -2.0));
        System.out.printf("The error on this is %.3f.\n", halfLifeError);
    }
    
    
    public static void main(String[] args)
    {
        //User input
        Scanner keyboard = new Scanner(System.in);
        
        //Ask user for a size of an NxN array for the radioactive particles
        System.out.println("Enter a size for the grid");
        int gridSize = keyboard.nextInt();
        
        //Speed the simulation up by a factor k, 25 minutes is a bit long for an experiment
        System.out.println("Enter a factor for the speed multiplier");
        double speedFactor = keyboard.nextDouble();
        
        //Ask user for a half-life for the radioactive particles, all identical so static
        System.out.println("Enter a halflife for the isotope in seconds");
        double halfLife = keyboard.nextDouble();
        
        
        //N = N(0) e ^(-lt) is the exponential decay
        //l = ln(2)/halflife, the decay constant is useful in the decay probability
        double decayConstant = Math.log(2)/halfLife;
        
        //Number of seconds per collection interval
        System.out.println("Enter the time between each data collection");
        int dataCollectionInterval = keyboard.nextInt();
        
        keyboard.close();
        
        RadioactiveMain experiment = new RadioactiveMain(gridSize, decayConstant,dataCollectionInterval);        
        
        experiment.run(speedFactor);
    }

}
