package fbalm.checkpoints.radioactivedecay;

import java.util.Random;

/**
 * A radioactive particle that can decay for the simulation
 * @author Floris Balm
 */
public class RadioactiveParticle {
    
    //Random number generator for the decay of particles
    private static Random generator;   
    
    private static double decayConstant;
    private int state;
    
    static {
        //Static generator to prevent identical seeds
        generator = new Random(System.currentTimeMillis());
    }
    
    /**
     * Constructor for the RadioactiveParticle class. Sets the initial state to 0
     */
    public RadioactiveParticle()
    {
        this.state = 0;
    }
    
    @Override
    /**
     * @return The current state as a string
     */
    public String toString()
    {
        return Integer.toString(state);
    }
    
    /**
     * @return Whether a particle has decayed
     */
    public boolean hasDecayed()
    {
        // A state of 1 is a decayed particle
        return (state==1)?true:false;
    }
    
    /**
     * Goes through a certain amount of time, and associates a probability of decay to that. 
     * @param timeStep seconds that have passed in the interval. Using that the probability of a particle
     *         surviving is e^(-lt) the probability we get a chance that a particle decays
     * @return Whether a particle has decayed.
     */
    public boolean decays(double timeStep)
    {
        //e^(-lt) of the population will have survived. Since this is always in range (0,1), 
        //this can be interpreted as the chance of survival
        double probabilityOfSurvival = Math.exp(-decayConstant*timeStep);
        
        if(generator.nextDouble() > probabilityOfSurvival)
        {
            state = 1; //1 means it has decayed
            return true;
        } else { return false;}
    }
    
    /**
     * Set the state to a new state
     * @param newState New state of the particle
     */
    public void setState(int newState)
    {
        state = newState;
    }
    
    
    @Override
    /**
     * Clone the particle to a new particle to keep encapsulation
     */
    public RadioactiveParticle clone()
    {
        RadioactiveParticle ret = new RadioactiveParticle();
        //0 is a not-decayed particle
        ret.setState(0);
        return ret;
        
    }
    /**
     * Set the decayconstant for the class
     * @param constant Decayconstant for the class
     */
    public static void setDecayConstant(double constant)
    {
        decayConstant = constant;
    }
    
}

